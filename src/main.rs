pub mod api;
pub mod cli;
pub mod models;
pub mod repositories;
pub mod utils;

#[tokio::main]
async fn main() {
    utils::db::init_db().await;
    utils::db::migrate_db().await;
    cli::cli::run().await;
}

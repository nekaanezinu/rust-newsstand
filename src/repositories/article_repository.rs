use chrono::Utc;

use crate::{
    api,
    models::{self, Article},
    utils::coversions,
    utils::db::db,
};

use super::articles::{self, mutations};

pub enum QueryActions {
    Find(i32),
    FindBy(Vec<(String, String)>),
}

pub enum QueryManyActions {
    Fetch(Option<u8>),
    Top(articles::queries::TopArticleFilters),
}

pub async fn query_one(action: QueryActions) -> Result<Article, sqlx::Error> {
    match action {
        QueryActions::Find(id) => articles::queries::find(id).await,
        QueryActions::FindBy(pairs) => articles::queries::find_by(pairs).await,
    }
}

pub async fn query(action: QueryManyActions) -> Result<Vec<models::Article>, sqlx::Error> {
    match action {
        QueryManyActions::Fetch(page) => articles::queries::fetch_articles(page).await,
        QueryManyActions::Top(filter) => articles::queries::fetch_top_articles(filter).await,
    }
}

pub async fn save_articles() {
    let mut stories = match api::hn::fetch_top_stories().await {
        Ok(stories) => stories,
        Err(e) => panic!("{:?}", e),
    };

    stories.reverse();

    for story in stories {
        let article = article_from_story(story);
        mutations::upsert(&article)
            .await
            .expect("Failed to save article");
    }
}

pub async fn partial_update_articles() -> Result<(), Box<dyn std::error::Error>> {
    let story_ids = api::hn::fetch_top_ids(Some(10)).await?;

    let ids_for_query: Vec<String> = story_ids.iter().map(|id| format!("'{}'", id)).collect();
    let sql = format!(
        "SELECT * FROM articles WHERE source = 'hn' AND external_id IN ({})",
        ids_for_query.join(",")
    );
    // println!("{:?}", sql);

    let db_pool = db().await?;
    let existing_articles: Vec<models::Article> = sqlx::query_as(&sql).fetch_all(&db_pool).await?;

    for story_id in story_ids {
        let matching_article = existing_articles
            .iter()
            .find(|a| a.external_id.parse::<i32>().unwrap_or_default() == story_id);
        match matching_article {
            Some(article) => mutations::update(article.id.unwrap()).await?,
            None => create_article(story_id).await?,
        };
    }
    Ok(())
}

async fn create_article(story_id: i32) -> Result<Article, sqlx::Error> {
    let story = api::hn::fetch_story(&story_id).await;
    let new_article = article_from_story(story.unwrap());
    mutations::upsert(&new_article).await
}

fn article_from_story(story: api::hn::Item) -> models::Article {
    models::Article {
        id: None,
        title: story.title,
        url: story.url,
        published_at: coversions::convert_unix_to_datetime(story.time),
        created_at: Utc::now(),
        source: "hn".to_string(),
        external_id: story.id.to_string().clone(),
        order_date: Utc::now(),
        score: story.score,
    }
}

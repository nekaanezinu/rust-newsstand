use crate::{
    models::{self, Article},
    utils::{self, db::db},
};

pub async fn find(id: i32) -> Result<Article, sqlx::Error> {
    let query = "SELECT * FROM articles WHERE id = ?";
    sqlx::query_as::<_, Article>(query)
        .bind(id)
        .fetch_one(&db().await?)
        .await
}

pub async fn find_by(pairs: Vec<(String, String)>) -> Result<Article, sqlx::Error> {
    let columns: Vec<_> = normalize_param_columns(&pairs);
    let sql = format!("SELECT * FROM articles WHERE {}", columns.join(" AND "));

    let mut query = sqlx::query_as::<_, Article>(&sql);
    for pair in pairs {
        query = query.bind(pair.1);
    }

    query.fetch_one(&db().await?).await
}

pub async fn fetch_articles(page: Option<u8>) -> Result<Vec<models::Article>, sqlx::Error> {
    let page = page.unwrap_or(1);
    let query = "
      SELECT * FROM articles ORDER BY order_date DESC LIMIT 10 OFFSET ?
    ";
    sqlx::query_as::<_, Article>(query)
        .bind((page - 1) * 10)
        .fetch_all(&db().await?)
        .await
}

pub enum TopArticleFilters {
    Day,
    Week,
    Month,
}

pub async fn fetch_top_articles(filter: TopArticleFilters) -> Result<Vec<Article>, sqlx::Error> {
    let from = match filter {
        TopArticleFilters::Day => utils::time::days_ago(1),
        TopArticleFilters::Week => utils::time::days_ago(7),
        TopArticleFilters::Month => utils::time::days_ago(30),
    };
    let to = utils::time::today();

    let query = "
      SELECT * FROM articles
      WHERE published_at >= ? AND published_at < ?
      ORDER BY score DESC
      LIMIT 10
    ";

    sqlx::query_as::<_, Article>(query)
        .bind(from)
        .bind(to)
        .fetch_all(&db().await?)
        .await
}

fn normalize_param_columns(pairs: &Vec<(String, String)>) -> Vec<String> {
    pairs
        .iter()
        .map(|(first, _second)| first.to_owned() + " = ? ")
        .collect()
}

use crate::{models::Article, utils::db::db};

use super::queries::find;

pub async fn upsert(article: &Article) -> Result<Article, sqlx::Error> {
    match article.id {
        Some(id) => update(id).await,
        None => create(article).await,
    }
}

pub async fn create(article: &Article) -> Result<Article, sqlx::Error> {
    let insert_query = Article::prepare_insert_query(article);
    let result = insert_query.execute(&db().await?).await?;

    find(result.last_insert_rowid() as i32).await
}

// more like bump for now
pub async fn update(id: i32) -> Result<Article, sqlx::Error> {
    let update_query = Article::prepare_update_query(&id);
    update_query.execute(&db().await?).await?;

    find(id).await
}

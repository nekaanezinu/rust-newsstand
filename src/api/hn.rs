use reqwest::Error;
use serde::Deserialize;

const API_URL: &str = "https://hacker-news.firebaseio.com/v0/";
const TOPSTORIES_ENDPOINT: &str = "topstories";
const ENDPOINT_FORMAT: &str = ".json";

#[derive(Deserialize, Debug)]
pub struct Item {
    pub id: i32,
    pub title: String,
    pub url: Option<String>,
    pub score: i32,
    pub time: u64,
    #[serde(rename = "type")]
    pub item_type: String,
}

pub async fn fetch_top_stories() -> Result<Vec<Item>, Error> {
    let url = format!("{}{}{}", API_URL, TOPSTORIES_ENDPOINT, ENDPOINT_FORMAT);
    let top_stories: Vec<i32> = reqwest::get(url).await?.json().await?;

    let mut stories = vec![];

    for &story_id in top_stories.iter().take(10) {
        match fetch_story(&story_id).await {
            Ok(story) => stories.push(story),
            Err(e) => println!("{}", e),
        }
    }

    Ok(stories)
}

pub async fn fetch_story(id: &i32) -> Result<Item, reqwest::Error> {
    reqwest::get(&format!(
        "https://hacker-news.firebaseio.com/v0/item/{}.json",
        id
    ))
    .await?
    .json()
    .await
}

pub async fn fetch_top_ids(amount: Option<usize>) -> Result<Vec<i32>, reqwest::Error> {
    let url = format!("{}{}{}", API_URL, TOPSTORIES_ENDPOINT, ENDPOINT_FORMAT);

    let ids = reqwest::get(&url).await?.json::<Vec<i32>>().await?;

    let limit = amount.unwrap_or(10);
    let top_ids = ids.into_iter().take(limit).collect::<Vec<i32>>();

    Ok(top_ids)
}

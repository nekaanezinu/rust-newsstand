use crate::utils::serializable::Serializable;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::query::Query;
use sqlx::sqlite::{SqliteArguments, SqliteRow};
use sqlx::{FromRow, Row, Sqlite};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Article {
    pub id: Option<i32>,
    pub title: String,
    pub url: Option<String>,
    pub created_at: DateTime<Utc>,
    pub published_at: DateTime<Utc>,
    pub external_id: String,
    pub source: String,
    pub order_date: DateTime<Utc>,
    pub score: i32,
}

impl Serializable for Article {
    fn to_json(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }
}

impl Article {
    pub fn prepare_insert_query(article: &Article) -> Query<'_, Sqlite, SqliteArguments<'_>> {
        let insert_query = "
            INSERT INTO articles
              (title, url, created_at, published_at, source, external_id, order_date, score)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?)
          ";
        sqlx::query(insert_query)
            .bind(&article.title)
            .bind(&article.url)
            .bind(article.created_at)
            .bind(article.published_at)
            .bind("hn")
            .bind(&article.external_id)
            .bind(article.order_date)
            .bind(article.score)
    }
    pub fn prepare_update_query(id: &i32) -> Query<'_, Sqlite, SqliteArguments<'_>> {
        let update_query = "
          UPDATE articles SET order_date = ?
          WHERE id = ?
        ";

        sqlx::query(update_query).bind(Utc::now()).bind(id)
    }
}

impl<'r> FromRow<'r, SqliteRow> for Article {
    fn from_row(row: &'r SqliteRow) -> Result<Article, sqlx::Error> {
        let id = row.try_get("id")?;
        let title = row.try_get("title")?;
        let url = row.try_get("url")?;
        let created_at = row.try_get("created_at")?;
        let published_at = row.try_get("published_at")?;
        let external_id = row.try_get("external_id")?;
        let source = row.try_get("source")?;
        let order_date = row.try_get("order_date")?;
        let score = row.try_get("score")?;

        Ok(Article {
            id,
            title,
            url,
            created_at,
            published_at,
            source,
            external_id,
            order_date,
            score,
        })
    }
}

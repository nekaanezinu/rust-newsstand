use crate::models;

pub fn print_articles(articles: Vec<models::Article>) {
    println!("{:?}", serde_json::to_string(&articles).unwrap());
}

use std::str::FromStr;

use crate::repositories::article_repository;

#[derive(Debug, Clone)]
pub enum UpdateMode {
    Skip,
    Partial,
    Full,
}

impl FromStr for UpdateMode {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "skip" => Ok(UpdateMode::Skip),
            "partial" => Ok(UpdateMode::Partial),
            "full" => Ok(UpdateMode::Full),
            _ => Ok(UpdateMode::Partial),
        }
    }
}

pub async fn update(update_mode: UpdateMode) {
    match update_mode {
        UpdateMode::Skip => {}
        UpdateMode::Partial => partial_update_articles().await,
        UpdateMode::Full => save_articles().await,
    }
}

pub async fn save_articles() {
    article_repository::save_articles().await;
}

pub async fn partial_update_articles() {
    match article_repository::partial_update_articles().await {
        Ok(_) => {}
        Err(e) => panic!("{}", e),
    };
}

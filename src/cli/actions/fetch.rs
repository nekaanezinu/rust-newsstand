use std::str::FromStr;

use crate::{
    models::Article,
    repositories::{
        article_repository::{self, QueryManyActions},
        articles::queries::TopArticleFilters,
    },
};

#[derive(Debug, Clone)]
pub enum TopQueryMode {
    Day,
    Week,
    Month,
    None,
}

impl FromStr for TopQueryMode {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "day" => Ok(TopQueryMode::Day),
            "week" => Ok(TopQueryMode::Week),
            "month" => Ok(TopQueryMode::Month),
            _ => Ok(TopQueryMode::None),
        }
    }
}

pub async fn articles(page: u8, top_mode: TopQueryMode) -> Option<Vec<Article>> {
    match top_mode {
        TopQueryMode::None => default(page).await,
        _ => top(top_mode).await,
    }
}

async fn default(page: u8) -> Option<Vec<Article>> {
    match article_repository::query(QueryManyActions::Fetch(Some(page))).await {
        Ok(articles) => Some(articles),
        Err(_) => None,
    }
}

async fn top(mode: TopQueryMode) -> Option<Vec<Article>> {
    let filter = match mode {
        TopQueryMode::Day => TopArticleFilters::Day,
        TopQueryMode::Week => TopArticleFilters::Week,
        TopQueryMode::Month => TopArticleFilters::Month,
        _ => TopArticleFilters::Day,
    };
    match article_repository::query(QueryManyActions::Top(filter)).await {
        Ok(articles) => Some(articles),
        Err(_) => None,
    }
}

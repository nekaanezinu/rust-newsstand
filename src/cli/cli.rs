use clap::Parser;

use crate::{cli::printer, models::Article};

use super::actions::{
    fetch,
    fetch::TopQueryMode,
    mutate::{self, UpdateMode},
};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Page number, defaults to 1.
    #[arg(short, long, default_value = "1")]
    page: u8,

    /// Update mode:
    ///
    /// full - update all articles
    ///
    /// partial - update only missing articles
    ///
    /// skip - do not update any articles
    #[clap(short, long, default_value = "partial")]
    update_mode: UpdateMode,

    /// Top query - fetches top scored articles in timeframe
    ///
    /// available options: day, week, month, none
    #[clap(short, long, default_value = "none")]
    top_query_mode: TopQueryMode,
}

pub async fn run() {
    let args = Args::parse();

    mutate::update(args.update_mode).await;

    match fetch::articles(args.page, args.top_query_mode).await {
        Some(articles) => print_articles(articles),
        None => panic!("failed to fetch articles"),
    }
}

fn print_articles(articles: Vec<Article>) {
    printer::print_articles(articles);
}

use async_trait::async_trait;

#[async_trait]
pub trait Queryable<T> {
    async fn upsert(&self) -> Result<T, sqlx::Error>;
}

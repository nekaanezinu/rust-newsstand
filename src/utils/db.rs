use core::panic;
use memoize::memoize;
use sqlx::{migrate::MigrateDatabase, Sqlite, SqlitePool};
use std::io::Error;
use std::string::String;

const DB_FILENAME: &str = "newsstand.sqlite";

pub async fn db() -> Result<SqlitePool, Error> {
    // println!("Opening database at {}", db_path());
    match SqlitePool::connect(&db_path()).await {
        Ok(pool) => Ok(pool),
        Err(e) => panic!("Could not connect to database: {}", e),
    }
}

pub async fn init_db() {
    if Sqlite::database_exists(&db_path()).await.unwrap_or(false) {
        // println!("Db already exists");
    } else {
        // println!("Creating db {:?}", db_path());
        match Sqlite::create_database(&db_path()).await {
            Ok(_) => (),
            Err(error) => panic!("Could not create database: {}", error),
        }
    }
}

pub async fn migrate_db() {
    let migrations = std::path::Path::new(&current_dir()).join("./migrations");
    match sqlx::migrate::Migrator::new(migrations)
        .await
        .unwrap()
        .run(&db().await.unwrap())
        .await
    {
        Ok(_) => (), // println!("Migration success"),
        Err(error) => {
            panic!("error: {}", error);
        }
    }
}

#[memoize]
fn db_path() -> String {
    current_dir() + &String::from("/".to_owned() + DB_FILENAME)
}

fn current_dir() -> String {
    let path = std::env::current_exe().expect("Could not get current executable path");
    path.parent()
        .expect("Failed to get directory of the executable")
        .display()
        .to_string()
}

pub trait Serializable {
    fn to_json(&self) -> String;
}

use chrono::{DateTime, Utc};

pub fn today() -> DateTime<Utc> {
    Utc::now()
}

pub fn days_ago(day_count: i32) -> DateTime<Utc> {
    today() - chrono::Duration::days(day_count as i64)
}

use chrono::{DateTime, NaiveDateTime, Utc};

pub fn convert_unix_to_datetime(timestamp: u64) -> DateTime<Utc> {
    // UNIX timestamp is in seconds and chrono::NaiveDateTime::from_timestamp accepts it in seconds
    match NaiveDateTime::from_timestamp_opt(timestamp as i64, 0) {
        Some(date) => DateTime::<Utc>::from_utc(date, Utc),
        None => panic!("canot convert timestamp"),
    }
}

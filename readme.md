```
Usage: newsstand [OPTIONS] --top-query-mode <TOP_QUERY_MODE>

Options:
  -p, --page <PAGE>
          Page number, defaults to 1

          [default: 1]

  -u, --update-mode <UPDATE_MODE>
          Update mode:

          full - update all articles

          partial - update only missing articles

          skip - do not update any articles

          [default: partial]

  -t, --top-query-mode <TOP_QUERY_MODE>
          Top query - fetches top scored articles in timeframe

          available options: day, week, month

  -h, --help
          Print help (see a summary with '-h')

  -V, --version
          Print version
```
